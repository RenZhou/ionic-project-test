import {Component} from '@angular/core';
import {NavController, AlertController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export class User {
  name: string;
  phone: string;
  email: string;
}

@Component({
  selector: 'page-inscription',
  templateUrl: 'inscription.html'
})
export class InscriptionPage {

  private myForm: FormGroup;

  private user: User = new User();

  constructor(public navCtrl: NavController, private fb: FormBuilder, public alertCtrl: AlertController) {
    this.myForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*'), Validators.minLength(4), Validators.maxLength(30)])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern('^(0|\\+33)[1-9]([0-9]{8})$')])],
      email: ['', Validators.compose([Validators.required, Validators.pattern(('^[a-z][a-zA-Z0-9_]*(\\.[a-zA-Z][a-zA-Z0-9_]*)?@[a-z][a-zA-Z-0-9]*\\.[a-z]+(\\.[a-z]+)?$'))])]
    });
  }

  public signUpAction() {
    const alert = this.alertCtrl.create({
      title: 'Sign Up',
      subTitle: 'You are now sign up' + this.user.name + this.user.phone + this.user.email,
      buttons: ['Confirm']
    });
    alert.present();
  }
}
